package learnInt;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class Interview_prc {

	public static void main(String[] args) {

		Interview_prc ip = new Interview_prc();
		// ip.ptrn();
		// ip.ptrn2();
		// ip.ptrn3();
		// ip.ptrn4();
//		System.out.println();
		// ip.ptrn5();
		// ip.ptrn6();
		// ip.ptrn7();
		// ip.ptrn8();
//		 ip.leapyear();
		// ip.swap();
		// ip.sorting();
		// ip.mail();
		// ip.pattern();
		// ip.patternX();
		// ip.matmul();
//		ip.BblSorting();
//		ip.pattern_Pyramid(5);
//		System.out.println();
//		ip.pattern_upsidedown(5);
//		ip.sorting_practice();
//		ip.nonrepeatingchar(); //under test
//		ip.str2ltrsrch();
//		ip.firstrepeatedchars();
//		ip.stringReverseWorkoutandcaps();
//		ip.the3dmatrix();
	}

	private void the3dmatrix() {
		// TODO Auto-generated method stub

		Scanner scanner = new Scanner(System.in);

		// Get size of 3D array from user
		System.out.print("Enter size for X dimension: ");
		int x = scanner.nextInt();
		System.out.print("Enter size for Y dimension: ");
		int y = scanner.nextInt();
		System.out.print("Enter size for Z dimension: ");
		int z = scanner.nextInt();

		// Create 3D array
		String[][][] arr = new String[x][y][z];

		// Take string input from user to populate array
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				for (int k = 0; k < z; k++) {
					System.out.print("Enter string for " + i + "," + j + "," + k + ": ");
					arr[i][j][k] = scanner.next();
				}
			}
		}

		// Print array
		System.out.println("3D array contains:");
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				for (int k = 0; k < z; k++) {
					System.out.print(arr[i][j][k] + " ");
				}
				System.out.println();
			}
		}

	}

	private void stringReverseWorkoutandcaps() {

		String original = "Hello";
		String reversed = "";

		for (int i = original.length() - 1; i >= 0; i--) {
			reversed = reversed + original.charAt(i);
		}

		System.out.println("Original: " + original);
		System.out.println("Reversed: " + reversed);

		String s = "   How are you   ";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ') {
				for (int j = i; j < s.length(); j++) {
					System.out.print(s.charAt(j));
				}
				break;
			}
		}

		String name = "yogeshwaran";

		for (int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if (i == 0 || i == name.length() - 1) {
				ch = (char) (ch - 32);
				System.out.print(ch);
			} else {
				char ch2 = name.charAt(i + 1);
				if (ch2 == ' ') {
					ch = (char) (ch - 32);
					System.out.print(ch);
					// i++;
				}

				else {
					ch2 = name.charAt(i - 1);
					if (ch2 == ' ') {
						ch = (char) (ch - 32);
						System.out.print(ch);
						// i++;
					} else
						System.out.print(ch);
				}
			}
		}

	}

	private void firstrepeatedchars() {

		char[] name = { 's', 'a', 'n', 't', 'h', 'o', 's', 'h' };
		int j;
		boolean present = false;
		for (j = 0; j < name.length; j++) {

			char ch = name[j];
			for (int i = j + 1; i < name.length; i++) {
				if (ch != name[i])
					continue;
				else {
					System.out.println("First repeated char --> " + ch);
					present = true;
					break;
				}
			}

			if (present == true) {
				break;

			}
			if (j == name.length) {

				System.out.println("no repeated chars");

			}

		}

	}

	private void str2ltrsrch() {
		// TODO Auto-generated method stub

		String s1 = "Today is monday";
		String s2 = "monday";
		char ch = s2.charAt(0);
		for (int i = 0; i <= s1.length() - 1; i++) {
			if (ch == s1.charAt(i)) {
				int count = 0;
				for (int j = 0; j < s2.length(); j++) {
					if (s2.charAt(j) == s1.charAt(i)) {
						count++;
						i++;
					}
				}
				if (count == s2.length()) {
					System.out.println("contains");
					break;
				} else {
					System.out.println("not contains");
				}
			}
		}

	}

	private void nonrepeatingchar() {

		String name = "abadefd";
		char temp = '*';
		int a = 0, count = 0;
		while (a < name.length()) {
			int b = 0;
			while (b <= name.length() - 1) {
				if (name.charAt(a) == name.charAt(a + 1)) {
					System.out.println(name.charAt(a) + " repeated");
					temp = name.charAt(b);
					count++;
					break;
				}
				b++;
			}
			a++;
		}

		if (count == 0) {
			System.out.println("-1");
		}

	}

	private void sorting_practice() {
		// TODO Auto-generated method stub
		int arr[] = { 8, 3, 0, 1, 6, 3, 99, 80, 5000 };
		int j = 1;
		while (j <= arr.length) {

			int i = 0;
			while (i < arr.length - j) {
				if (arr[i] > arr[i + 1]) {
					int temp = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = temp;
				}
				i++;
			}
			j++;
		}
		for (int k = 0; k < arr.length - 1; k++) {
			System.out.print(arr[k] + " ");
		}

	}

	private void pattern_upsidedown(int no) {
		// TODO Auto-generated method stub
		for (int row = 2; row <= no; row++) {
			for (int spaces = 1; spaces < row; spaces++) {
				System.out.print("  ");
			}
			for (int col = row; col <= 5; col++) {
				System.out.print("*" + " ");
			}

			for (int col = 4; col >= row; col--) {
				System.out.print("*" + " ");
			}
			System.out.println();

		}
	}

	private void pattern_Pyramid(int no) {
		// TODO Auto-generated method stub
		for (int row = 1; row <= no; row++) {

			for (int spaces = 0; spaces < no - row; spaces++) {
				System.out.print("  ");
			}

			for (int col = row; col >= 1; col--) {
				System.out.print("*" + " ");
			}
			for (int col = 2; col <= row; col++) {
				System.out.print("*" + " ");
			}
			System.out.println();
		}
	}

	private void BblSorting() {

		int[] ar = { 40, 50, 20, 30, 10 };
		int j = 1;

		while (j <= ar.length) {
			int i = 0;

			while (i < ar.length - j) {
				if (ar[i] > ar[i + 1]) {
					int temp = ar[i];
					ar[i] = ar[i + 1];
					ar[i + 1] = temp;
				}
				i++;
			}
			j++;

		}
		System.out.println("Third Smallest--->" + ar[2]);
		for (int k = 0; k < ar.length; k++) {
			System.out.print(ar[k] + " ");
		}
	}

	private void matmul() {

		int row1, col1, row2, col2;
		Scanner s = new Scanner(System.in);

		System.out.print("Enter number of rows in first matrix:");
		row1 = s.nextInt();

		System.out.print("Enter number of columns in first matrix:");
		col1 = s.nextInt();

		System.out.print("Enter number of rows in second matrix:");
		row2 = s.nextInt();

		System.out.print("Enter number of columns in second matrix:");
		col2 = s.nextInt();

		if (col1 != row2)

		{
			System.out.println("Matrix multiplication is not possible");
		}

		else {

			int a[][] = new int[row1][col1];
			int b[][] = new int[row2][col2];
			int c[][] = new int[row1][col2];

			System.out.println("Enter values for matrix A: \n");

			for (int i = 0; i < row1; i++)

			{

				for (int j = 0; j < col1; j++)

				{

					a[i][j] = s.nextInt();
				}

			}

			System.out.println("Enter values for matrix B : \n");
			for (int i = 0; i < row2; i++)

			{

				for (int j = 0; j < col2; j++)

				{
					b[i][j] = s.nextInt();

				}

			}

			System.out.println("Matrix multiplication is: \n");

			for (int i = 0; i < row1; i++)

			{

				for (int j = 0; j < col2; j++) {

					c[i][j] = 0;

					for (int k = 0; k < col1; k++) {

						c[i][j] += a[i][k] * b[k][j];

					}
					System.out.print(c[i][j] + " ");

				}
				System.out.println();
			}
		}
	}

	private void patternX() {
		for (int row = 1; row <= 4; row++) {
			for (int col = 1; col < row; col++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= 6 - row; i++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		for (int row = 5; row >= 1; row--) {
			for (int col = 1; col < row; col++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= 6 - row; i++) {
				System.out.print("*" + " ");
			}
			System.out.println();
		}
	}

	private void pattern() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	private void mail() {
		String mail = "test123@gmail.com";
		for (int i = 0; i < mail.length(); i++) {
			if (mail.charAt(i) >= 'a' && mail.charAt(i) <= 'z') {
				System.out.print("*");
			} else {
				System.out.print(mail.charAt(i));
			}
		}

	}

	private void sorting() {
		int[] arr = { 64, 34, 25, 12, 22, 11, 90 };

		// Perform bubble sort
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = 0; j < arr.length - i - 1; j++) {
				if (arr[j] > arr[j + 1]) {
					// swap arr[j] and arr[j+1]
					int temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}

		// Display sorted array
		System.out.println(Arrays.toString(arr));
	}

	private void swap() {
		int[] arr = { 3, 2, 1, 7 };
		arr[0] = arr[0] + arr[3];
		arr[3] = arr[0] - arr[3];
		arr[0] = arr[0] - arr[3];

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}

	private void leapyear() {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a valid year :");
		int sc1 = sc.nextInt();

		int leap_year = sc1;
		if ((leap_year % 400 == 0 && leap_year % 100 != 0) || leap_year % 4 == 0) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}

	}

	private void ptrn8() {
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= 5; col++) {
				if (row + col == 6 || row + col == 7 || row + col == 8 || row + col == 9 || row + col == 10) {
					System.out.print(col + " ");
				} else {
					System.out.print("* ");
				}
			}
			System.out.println();
		}

	}

	private void ptrn7() {
		// TODO Auto-generated method stub
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		for (int row = 1; row <= 4; row++) {
			for (int col = 4; col >= row; col--) {
				System.out.print("* ");
			}
			System.out.println();
		}

	}

	private void ptrn6() {

		String str = "mom", str2 = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			// System.out.print(str.charAt(i));
			// System.out.println();
			str2 += str.charAt(i);

		}
		for (int i = 0; i <= str2.length() - 1; i++) {
			if (str.charAt(i) != str2.charAt(i)) {
				System.out.println("not a palin");
				break;
			}
			// System.out.print(str2.charAt(i));
			// System.out.println(str.charAt(i));
		}

	}

	private void ptrn5() {
		char a = 'A';
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row + 3; col++) {
				if (a >= 'A' && a <= 'Z') {
					System.out.print((int) a + " " + a + " ");
					a++;
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}

	}

	private void ptrn4() {

		char a = 'a';
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row + 3; col++) {
				if (a >= 'a' && a <= 'z') {
					System.out.print((int) a + " " + a + " ");
					a++;
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}

	}

	private void ptrn3() {
		int a = 1;
		for (int row = 1; row <= 3; row++) {
			for (int col = 1; col <= 3; col++) {
				if (row % 2 != 0) {
					System.out.print(a + " ");
				} else {
					System.out.print(" ");
				}
				a++;
			}
			System.out.println();
		}

	}

	private void ptrn2() {
		int row = 1;
		while (row <= 7) {
			for (int col = 1; col <= 7; col = col + 2) {
				if (row % 2 != 0) {
					System.out.print(row + " ");
				} else {
					System.out.print("* ");
				}
			}
			System.out.println();
			row++;
		}

	}

	private void ptrn() {

		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print(row + " ");
			}
			System.out.println();
		}

	}

}
