package learnregex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {

	public static void main(String[] args) {

		RegexDemo rd = new RegexDemo();
		rd.type2();
		rd.type3();
		rd.type1();
		rd.type4();
		rd.type5();

	}

	private void type5() {
		
		String pattern = "-";
	       String input = "28-March-2023";
	            Pattern patternObj = Pattern.compile(pattern);
	            String[] items = patternObj.split(input);
	            for(int i=0;i<items.length;i++)
	            {
	              System.out.println(items[i]);
	            }


		
	}

	private void type4() {
		
		String mobile = "9884012810";
		  Pattern patternObj = Pattern.compile("[6-9][0-9]{9}"); // means first number should be 6-9 and middle is 0-9 and {9} total nine numbers 
		  Matcher matcherObj = patternObj.matcher(mobile);
		    while(matcherObj.find())
		    {
		      System.out.print(matcherObj.group());
		    }
		
	}

	private void type1() {

		String input = "$%^&*  (1234567890 My mobile number is 9856743211";

		// Pattern patternObj = Pattern.compile("\\d{2}"); //d-> digits {10}-> continues
		// 10 numbers
		// Pattern patternObj = Pattern.compile("[1234567890]");
		// Pattern patternObj = Pattern.compile("[6-9]"); //used to check how many
		// values are between 6 to 9
		// Pattern patternObj = Pattern.compile("[a-z]");
		// Pattern patternObj = Pattern.compile("[a-zA-Z]");  
		// Pattern patternObj = Pattern.compile("[^a-zA-Z]"); // used to print instead
		Pattern patternObj = Pattern.compile("[^a-zA-Z0-9 ]"); // used to omit all the caps and small letters and
																// numbers and space print spl characters
		Matcher matcherObj = patternObj.matcher(input);
		while (matcherObj.find()) {
			System.out.println(matcherObj.group());

			System.out.println("Starting point" + " " + matcherObj.start());
			System.out.println("end point" + " " + matcherObj.end());

		}

	}

	private void type3() {

		String password = "Chennai is the capital city";
		// Pattern patternObj = Pattern.compile("\\s"); //used to count spaces
		// Pattern patternObj = Pattern.compile("\\S");// used to skip spaces
		Pattern patternObj = Pattern.compile("\\d");// used to print digits & D means not digits
		Matcher matcherObj = patternObj.matcher(password);
		int count = 0;
		while (matcherObj.find()) {
			count++;
			System.out.print(matcherObj.group());
		}
		System.out.println(count);

	}

	private void type2() {

		String password = "Chennai is the capital of TamilNadu";
		// Pattern patternObj = Pattern.compile("TamilNadu$"); // to check tn is printed
		// in end word
		Pattern patternObj = Pattern.compile("^Chennai"); // to check chennai is printed in starting word
		Matcher matcherObj = patternObj.matcher(password);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group());
		}
		
	}

}
