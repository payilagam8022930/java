package controlFlow;

public class SingleInheritance extends Si {

	public static void main(String[] args) {

		SingleInheritance si = new SingleInheritance();

		si.hi();
		si.bye();

	}

	public void hi() {
		System.out.println("hi");

	}

}
