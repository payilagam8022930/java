package prg_class;

public class StripTrailing {

	public static void main(String[] args) {

		StripTrailing at = new StripTrailing();
		at.sample();
		System.out.println();
		String s = "   How are you   ";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ') {
				for (int j = i; j < s.length(); j++) {
					System.out.print(s.charAt(j));
				}
				break;
			}
		}
		
		

	}

	private void sample() {

		String s = "   How are you   ";
		int first = 0, last = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ') {
				first = i;
				break;
			}
		}
		for (int i = s.length() - 1; i >= 0; i--) {
			if (s.charAt(i) != ' ') {
				last = i;
				break;
			}
		}
		for (int i = first; i <= last; i++) {
			System.out.print(s.charAt(i));
			
		}

	}
}