package prg_class;

public class Sample1 {

	public static void main(String[] args) {
		int a=5;
		System.out.println(4|5&3); 
		System.out.println(4&5&8);  //bit wise operator
		System.out.println("--------------------------");
		System.out.println(7>>2);
		System.out.println(4<<1); //shift
		System.out.println(a+=5);
		System.out.println(~5);  //tilde 
		System.out.println(5^4); //capsilon //xor operator
		System.out.println("---------------------------");
//	System.out.println(pp instanceof parent); used to check the parent 
		
		int no=3;  //only String int enum data type will be accepted
		switch(no) {
		case 7:
			System.out.println("hi");
		case 6:
			System.out.println("hello");
		case 5:
			System.out.println("greet");
		default :
			System.out.println("byee");
		}
		
		

	}

}
