package prg_class;

import java.lang.Math;

public class Sample2 {

	public static void main(String[] args) {

		Sample2 ss = new Sample2();

		ss.numSeries();
		ss.numSeries1();
		ss.numSeries2();
		ss.numSeries3();
		ss.findfactorial();

		// to iterate the arguments for find power
		// eg: ss.findpower(3,3);
		int no = 1;
		while (no <= 5) {
			ss.findpower(no, no); // to give arg to find power
			no = no + 1;
		}

	}

	private void findfactorial() {
		int no = 1, sum = 1;
		while (no <= 6) {
			sum = sum * no;
			no = no + 1;

		}
		System.out.println(sum + " <==factorial");
		System.out.println("---------------------------------------------------------------");
	}

	private void findpower(int base, int power) {
		int result = 1;
		while (power > 0) {
			result = result * base;
			power = power - 1;

		}
		System.out.println("Power value value is =>" + result);
		System.out.println("---------------------------------------------------------------");
	}

	private void numSeries3() {

		// sum of n numbers

		int no = 1, sum = 0;
		while (no <= 5) {
			sum = sum + no;
			no = no + 1;
		}
		System.out.println(sum + " <= sum of n numbers");
		System.out.println("---------------------------------------------------------------");
	}

	private void numSeries2() {

		int no = 1, so;
		while (no <= 6) {
			so = no * no;
			System.out.println(so);
			no++;
		}
		System.out.println("------------------------------------------------------");
	}

	private void numSeries1() {

		int no = 1, ko;

		while (no <= 5) {
			ko = no;
			System.out.println(Math.round(Math.pow(no, ko)));
			no++;
		}

		System.out.println("---------------------------------------------------------------");

	}

	private void numSeries() {

		int no = 1, so;
		while (no <= 6) {
			so = no * no * no;
			System.out.println(so);
			no++;
		}
		System.out.println("------------------------------------------------------------");
	}

}
