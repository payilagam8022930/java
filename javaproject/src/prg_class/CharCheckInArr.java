package prg_class;

public class CharCheckInArr {

	public static void main(String[] args) {

		char[] name = { 's', 'a', 'n', 't', 'h', 'o', 's', 'h' };
		int j;
		boolean present = false;
		for (j = 0; j < name.length; j++) {

			char ch = name[j];
			for (int i = j + 1; i < name.length; i++) {
				if (ch != name[i])
					continue;
				else {
					System.out.println("First repeated char --> " + ch);
					present = true;
					break;
				}
			}

			if (present == true) {
				break;

			}
			if (j == name.length) {

				System.out.println("no repeated chars");

			}

		}

	}

}
