package prg_class;

public class Patternprg {

	public static void main(String[] args) {
		Patternprg p = new Patternprg();

		// p.p1();
		// p.p2();
		// p.p3();
		// p.pI();
		// p.pC();
		// p.pO();
		p.pD();
		// p.pP();

	}

	private void pP() {
		pD();
		for (int r = 1; r <= 7; r++) {
			System.out.println("* ");
		}

	}

	private void pD() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1 || c == 7) {
					if (r == 1 && c == 7) {
						System.out.print("  ");
					} else if (r == 7 && c == 7) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pO() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1 || c == 7) {
					if (r == 1 && (c == 1 || c == 7)) {
						System.out.print("  ");
					} else if (r == 7 && (c == 1 || c == 7)) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pC() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 1) {
					if (r == 1 && c == 1) {
						System.out.print("  ");
					} else if (r == 7 && c == 1) {
						System.out.print("  ");
					} else {
						System.out.print("* ");
					}
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pI() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 4) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void p3() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7 || c == 4) {
					System.out.print("* ");
				} else {
					System.out.print("# ");
				}
			}
			System.out.println();
		}

	}

	private void p2() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				if (r == 1 || r == 7) {
					System.out.print("* ");
				} else {
					System.out.print("# ");
				}
			}
			System.out.println();
		}

	}

	private void p1() {
		for (int r = 1; r <= 7; r++) {
			for (int c = 1; c <= 7; c++) {
				System.out.print("# ");
			}
			System.out.println();
		}

	}

}