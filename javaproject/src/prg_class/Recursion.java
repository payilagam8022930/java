package prg_class;

public class Recursion {

	public static void main(String[] args) {
		Recursion rr = new Recursion();
		
		int sodresult = rr.sod(5687);
		System.out.println(sodresult);
		
		int result =find_fact(5);
		System.out.println(result);

	}



	private int sod(int value) {

		if (value == 0)
			return 0;
		else
			return value % 10 + sod(value / 10);

	}

	public static int find_fact(int i) {

		if (i == 1)
			return 1;
		else
			return i * find_fact(i - 1);

	}

}
