package prg_class;

public class Anagram {

	public static void main(String[] args) {

		String s1 = "CAT";
		char[] a = s1.toCharArray();
		String s2 = "TACC";
		int count = 0;
		char[] b = s2.toCharArray();
		for (int j = 0; j < a.length; j++) {
			char ch = a[j];
			for (int i = 0; i < b.length; i++) {
				if (ch == b[i]) {
					b[i] = '*';
					count++;
					break;
				}
			}

		}
		if (count == a.length) {
			System.out.println("ANAGARAM");
		} else {
			System.out.println("NOT ANAGRAM");
		}

	}

}
