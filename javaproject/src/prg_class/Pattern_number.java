package prg_class;

public class Pattern_number {

	public static void main(String[] args) {

		Pattern_number pn = new Pattern_number();
		pn.sample();
		System.out.println("-----------------");
		pn.sample1();
		System.out.println("-----------------");
		pn.sample2();
		System.out.println("-----------------");
		pn.sample3();
		System.out.println("------------------");
		pn.sample4();
		System.out.println("------------------");
		pn.sample5();
		System.out.println("------------------");
		pn.sample6();
		System.out.println("------------------");
		pn.sample7();
		System.out.println("------------------");
		pn.sample8();
		System.out.println("------------------");
		pn.sample9();
		System.out.println("------------------");
		pn.sample10();
		System.out.println("------------------");
		pn.sample11();
		System.out.println("------------------");
		pn.sample12();
		System.out.println("-------------------");
		pn.sample();
		System.out.println("-----------------");
		pn.sample13();
		System.out.println("-----------------");
		pn.sample14();
		System.out.println("-----------------");
		pn.sample15();
		System.out.println("-----------------");

	}

	private void sample15() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				if (col == row || col == 1 && row == 5 || row - col == 2)

					System.out.print(0 + " ");
				else
					System.out.print(1 + " ");
			}

			System.out.println();
		}

	}

	private void sample14() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				if (col % 2 == 0)

					System.out.print(1 + " ");
				else
					System.out.print(0 + " ");
			}

			System.out.println();
		}
	}

	private void sample13() {

		for (int row = 1; row <= 7; row += 2) {
			for (int col = 1; col <= row; col++) {
				System.out.print("*" + " ");

			}

			System.out.println();
		}

	}

	private void sample12() {

		for (int row = 5; row >= 1; row--) {
			for (int col = 1; col < row; col++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= 6 - row; i++) {
				System.out.print(i + " ");
			}
			System.out.println();
		}

	}

	private void sample11() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print(col + " ");
			}
			for (int i = 1; i <= 6 - row; i++) {
				System.out.print("  ");
			}
			System.out.println();
		}

	}

	private void sample10() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col < row; col++) {
				System.out.print("  ");
			}
			for (int i = 1; i <= 6 - row; i++) {
				System.out.print(i + " ");
			}
			System.out.println();
		}

	}

	private void sample9() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col < row; col++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= 6 - row; i++) {
				System.out.print("* ");
			}
			System.out.println();
		}

	}

	private void sample8() {

		int end = 1;
		for (int row = 1; row <= 5; row++) {
			for (int col = row; col <= end; col++) {
				System.out.print(col + " ");
			}
			System.out.println();
			end = end + 2;
		}
	}

	private void sample7() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				int a = row + col;
				System.out.print(a - 1 + " ");
			}
			System.out.println();
		}

	}

	private void sample6() {

		int no = 1;
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print(no++ + " ");
			}
			System.out.println();
		}

	}

	private void sample5() {

		for (char row = 'a'; row <= 'e'; row++) {
			for (char col = 'a'; col <= row; col++) {
				System.out.print("*" + " ");
			}
			System.out.println();
		}

	}

	private void sample4() {

		for (char row = 'a'; row <= 'e'; row++) {
			for (char col = 'a'; col <= row; col++) {
				System.out.print(col + " ");
			}
			System.out.println();
		}
	}

	private void sample3() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print(col + " ");
			}
			System.out.println();
		}

	}

	private void sample2() {

		for (int row = 1; row <= 5; row++) {
			for (int col = 5; col >= 6 - row; col--) {
				System.out.print(col + " ");
			}
			System.out.println();
		}

	}

	private void sample1() {
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print(col + " ");
			}
			System.out.println();
		}

	}

	private void sample() {

		int row = 5;
		while (row >= 1) {
			for (int col = 1; col <= row; col++) {
				System.out.print(col + " ");

			}
			System.out.println();
			row--;
		}

	}

}
