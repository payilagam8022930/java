package prg_class;

public class ArrprgShift {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrprgShift ad = new ArrprgShift();

		// ad.find_second_big();
		int[] a = { 5, 10, 15, 20 };
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
		for (int j = 1; j <= 2; j++) {
			a = ad.shift_left(a);
			for (int i = 0; i < a.length; i++) {
				System.out.print(a[i] + " ");
			}
			System.out.println();
		}

	}

	private int[] shift_left(int[] a) {
		// TODO Auto-generated method stub
		int temp = a[0];
		int i = 0;
		while (i < a.length - 1) {
			a[i] = a[i + 1];
			// System.out.print(a[i] + " ");
			i++;
		}
		a[i] = temp;
		// System.out.print(a[i] + " ");
		return a;

	}

}
