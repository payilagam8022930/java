package controlflow2;

public class Singleinheritance extends Si2 {

	public static void main(String[] args) {

		Singleinheritance si = new Singleinheritance();

		si.hi();
		si.bye();

	}

	public void hi() {
		System.out.println("hi");

	}

}
