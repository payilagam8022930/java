package learn_collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapDemo {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		
		HashMap hm=new HashMap();
		hm.put("pongal", 60);
		hm.put("idli",40);
		hm.put("briyani", 70);
		hm.put("Noodles", 90);
		hm.put("masal dosa", 70);
		System.out.println(hm);
		System.out.println(hm.get("Noodles"));
		System.out.println(hm.remove("Noodles"));
		System.out.println(hm);
		System.out.println(hm.containsKey("idli"));
		System.out.println(hm.containsValue(40));
		System.out.println(hm.putIfAbsent("Noodles", 90));
		System.out.println("--------------------------------------------------------------------");
		
		//key -> pongal idly briyani -> keyset
		//Value -> 90,50,76 -> Collection
		
		//key	value	key-value
		//pongal 60		Entry
		//idly	 70		Entry
		//briyani 50	Entry
		
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		System.out.println(hm.entrySet());
		
		System.out.println("------------------------------------------------------------------------------");
		
		Iterator i=hm.entrySet().iterator();  //this block used to print entire key and values seperately
		
		while(i.hasNext()) {
			//System.out.println(i.next());
			Entry me=(Map.Entry)i.next(); //down casting
			System.out.println(me.getKey());
			System.out.println(me.getValue());
			
			if(me.getKey().equals("pongal"))
				me.setValue((int)me.getValue()+5);  // to increase the price(value) of pongal +5
		}
		System.out.println("------------------------------------------------------------------");
		System.out.println(hm);
		
		
		
	}

}
