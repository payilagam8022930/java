package learn_collection;

import java.security.cert.CollectionCertStoreParameters;
import java.util.ArrayList;
import java.util.Collections;

public class ArrayListDemo {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
//---------------Wrapper class------------------------------
		
		// byte -> Byte
		// long -> Long
		// int ->Integer
		// char -> Character
		// boolean -> Boolean
		// short -> Short
		// double -> Double

		int no = 10;
		Integer n = no;
		System.out.println(n); // Auto boxing
		int no1 = n; // Auto unboxing
		
//---------------collection list---------------------------------
		
		ArrayList al = new ArrayList();

		al.add("santhosh");
		al.add("sownesh");
		al.add(10);
		al.add(true);
		al.add(5.5f);
		System.out.println(al);
		al.add("sangeetha");
		al.add(true);
		System.out.println(al);
		System.out.println(al.contains("yogi")); // there is no yogi in the list
		System.out.println(al.get(3));  // index value 
		System.out.println(al.indexOf(true));
		System.out.println(al.lastIndexOf(true));
		System.out.println(al.remove(2));
		System.out.println(al.set(3,"hi"));
		System.out.println("--------------------------------------------------------------------------");
		ArrayList al2=new ArrayList();
		
		al2.add(al);
		al2.add("yogesh");
		al2.add("vignesh");
		System.out.println("al2 ->"+al2);
		System.out.println(al2.contains(al));
//		System.out.println(al2.remove("vignesh"));
//		System.out.println(al2);
		System.out.println(al2.removeAll(al));
		System.out.println(al2);
		
		
		
	}

}
