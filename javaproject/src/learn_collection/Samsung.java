package learn_collection;

public class Samsung implements Comparable<Samsung>{

 int price,ram,mp;
 public Samsung(int price, int ram, int mp) {
  this.price=price;
  this.ram=ram;
  this.mp=mp;
 }

 public static void main(String[] args) {
 
  Samsung s1 = new Samsung(15000,8,24);
  Samsung s2 = new Samsung(15000,6,12);
  int result = s1.compareTo(s2);
  System.out.println(result);
  if(result>0) {
   System.out.println("s1 price high");
  }else if(result<0) {
   System.out.println("s2 price high");
  }else
   System.out.println("s1,s2 equal");
  //s1->caller object
  //s2->called object
 }

 @Override
 public int compareTo(Samsung s2) {
  if(this.price>s2.price)
  {
   if(this.ram>s2.ram) {
    return +14536;
   }
  }else if(this.price<s2.price) {
   return  -23542;
  }
  return 0;
 }

 //@Override
 //public int compareTo(Object o) {
 // Samsung s2 = (Samsung)o; //downCasting
 // return 0;
 //}

 

}