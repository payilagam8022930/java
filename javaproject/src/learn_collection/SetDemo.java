package learn_collection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;



public class SetDemo {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		

ArrayList dd=new ArrayList();
		
		dd.add("aravindh");
		dd.add("santhosh");
		dd.add("yogi");
		dd.add("hai");
		dd.add("santhi");
		//no duplicates
		//no order
		System.out.println(dd);
		HashSet hs=new HashSet(dd);
		System.out.println(hs);
		
		System.out.println("------------------------------------------------------------------------------------");
		
		for(Object obj: dd)
		{
			String str=(String)obj;
			if(str.startsWith("s"))
				System.out.println(obj);
		}
		
		System.out.println("--------------------------------------------------------------------------");
		
		Iterator i= dd.iterator();
		boolean result=i.hasNext();
		while(result) {
			System.out.println(i.next());
			result =i.hasNext();
		}
		
		System.out.println("--- TreeSet ----------------------------------------------------------------------------");
		//Tree set
		
TreeSet ss=new TreeSet();
		
		ss.add("aravindh");
		ss.add("santhosh");
		ss.add("yogi");
		ss.add("hai");
		ss.add("santhi");
		
		System.out.println(dd);
	}

}
