package looping;

public class Box {
	public static void main(String[] args) {

		Box b = new Box();

		b.box1();
		b.box2();

	}

	private void box2() {

		int rows = 5;
		int columns = 6;

		// Outer loop for rows
		for (int i = 1; i <= rows; i++) {

			// Inner loop for columns
			for (int j = 1; j <= columns; j++) {

				// Check if it's the first or last row, or the first or last column
				if (i == 1 || i == rows || j == 1 || j == columns) {

					System.out.print("+ ");
				} else {

					System.out.print("  "); // Print two spaces for the empty space inside the box
				}
			}

			System.out.println(); // Move to the next line after printing each row
		}
	}

	private void box1() {

		for (int l1 = 1; l1 <= 4; l1++) {
			System.out.print("+" + " ");
		}

		for (int l2 = 0; l2 <= 0; l2++)
			for (int l3 = 1; l3 <= 4; l3++) {
				System.out.println("+" + " ");
			}

		for (int l2 = 0; l2 <= 0; l2++)
			for (int l3 = 1; l3 <= 5; l3++) {
				System.out.print("+" + " ");
			}
		System.out.println("");
		System.out.println("");
	}
}
