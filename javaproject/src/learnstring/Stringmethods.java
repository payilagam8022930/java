package learnstring;

public class Stringmethods {

	public static void main(String[] args) {
		String name = "santhosh";
		String name1 = "santhosh";

		System.out.println(name1.length());
		for (int i = 0; i < name.length(); i++) {
			System.out.println(name.charAt(i));
		}
		System.out.println(name.startsWith("sa"));
		System.out.println(name.endsWith("hi"));

		String date = " 10 08 2001";
		String[] ar = date.split(" ");

		for (String str : ar) {
			System.out.println(str);
		}
		System.out.println(name.compareTo(name1));
		System.out.println(name1.compareToIgnoreCase(name));

		System.out.println(name.concat(name));
		System.out.println(name1.substring(4));
		System.out.println(name1.substring(2, 4));
		System.out.println(name1.contains("th"));
		System.out.println(name1.equals(name));
		System.out.println(name1.equalsIgnoreCase("SanThosH"));

		String name3 = "a";
		System.out.println(name3.isEmpty());
		// Santhosh
		System.out.println(name1.indexOf('s'));
		System.out.println(name1.lastIndexOf('s'));
		char[] ch = name1.toCharArray();
		System.out.println(ch);
		System.out.println(name1.toUpperCase());
		System.out.println(name1.toLowerCase());

		String name4 = " ras ika  ";
		System.out.println(name4.length());
		name = name4.trim();
		System.out.println(name.length());
		System.out.println(name);

	}

}
