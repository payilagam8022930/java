package learnstring;

import java.util.Iterator;

public class Stringbufferdemo {
	
	public static void main(String[] args) {
		String[] name ={"aravind","yogi","santhosh"};
	
		Stringbufferdemo sbd=new Stringbufferdemo();
		sbd.joinname(name);
	}

	private void joinname(String[] name) {
		String join="";
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<name.length;i++)
		{
			join=join+name[i];
			sb.append(name[i]);
			System.out.println("string-->"+join.hashCode());
			System.out.println("stringbuffer--->"+sb.hashCode());
			
		}
		System.out.println("------------------------------------------------------------");
		System.out.println(join);
		System.out.println(sb);
		System.out.println(); 
		System.out.println(sb.replace(0,0,"123456"));
		System.out.println();
		System.out.println(sb.insert(10,"123456"));
		System.out.println();
		String a="java";
		a=a.toUpperCase();
		System.out.println(a);
	}

}
