package learnarray2;

public class Arrprg {

	public static void main(String[] args) {

		Arrprg ar = new Arrprg();
		ar.min();
		System.out.println("-----------------------------");
		ar.biggest();
		System.out.println("-----------------------------");
		ar.smallest();
		System.out.println("-----------------------------");
		ar.shift();
		System.out.println("-----------------------------");
		ar.addarr();
		System.out.println("--------------------------------");

	}

	private void addarr() {
		
		int a[]= {5,10,15,50};
		int b[]= {15,10,5};
		
		
	}

	private void shift() {

		int a[] = { 4, 67, 98, 999, 99 };
		int temp = a[0];
		int i = 0;
		while (i < a.length - 1) {
			a[i] = a[i + 1];
			System.out.print(a[i] + " ");
			i++;
		}
		a[i] = temp;
		System.out.println(a[i] + " ");
		

	}

	private void smallest() {

		int[] a = { 4, 67, 98, 999, 99 };
		int min = Integer.MAX_VALUE;
		int smin = Integer.MAX_VALUE;
		int i = 0;
		while (i < a.length) {
			if (a[i] < min) {
				smin = min;
				min = a[i];
			} else if (a[i] < smin) {
				smin = a[i];
			}
			i++;
		}
		System.out.println(min + " " + smin);
	}

	private void biggest() {

		int a[] = { 4, 67, 98, 999, 99 };
		int i = 1, sbig = Integer.MIN_VALUE, big = Integer.MIN_VALUE;
		while (i < a.length) {

			if (a[i] > big) {
				sbig = big;
				big = a[i];
			} else if (a[i] > sbig) {

				sbig = a[i];

			}
			i++;
		}
		System.out.println(big + " " + sbig);

	}

	private void min() {

		int[] ar = { 150, 100, 129, 987, 100 };
		int min = ar[0];
		for (int i = 1; i < ar.length; i++) {
			if (ar[i] < min) {
				min = ar[i];
			}
		}
		System.out.println(min);

	}

}
