package learnpattern;

import java.util.Scanner;

public class Patterns1 {

	public static void main(String[] args) {
		Patterns1 lp = new Patterns1();
		
		  int startRange = 100;
	        int endRange = 500;

	        System.out.println("Prime numbers between " + startRange + " and " + endRange + ":");
	        for (int i = startRange; i <= endRange; i++) {
	            if (thatPrime(i)) {
	                System.out.print(i + " ");
	            }
	        }
		
		// lp.pattern();
		// lp.pattern1();
		/*
		 * int number =1111 ; // Replace with the number you want to check
		 * 
		 * if (isPrime(number)) { System.out.println(number + " is a prime number."); }
		 * else { System.out.println(number + " is not a prime number."); }
		 */
	// System.out.println("---------------------------------------------------------------");
	// lp.base();
		
		//quiz();

	}
	

//Method to check if a number is prime
	static boolean thatPrime(int num) {
		
		if (num <= 1) {
			return false;
		}
		for (int i = 2; i <= num / 2; i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
		
	}
	
//no of prime numbers
	
	  private static boolean isPrime(int number) {
	        if (number <= 1) {
	            return false;
	        }

	        for (int i = 2; i <= number/2; i++) {
	            if (number % i == 0) {
	                return false;
	            }
	        }
	        return true;
	    }

	static void quiz() {

		// count of duplicate elements

		int arr[] = { 10, 20, 30, 10, 20, 30, 50, 10, 40 };
		int freq[] = new int[arr.length];

//		System.out.println(arr.length);
//		System.out.println(freq.length);

		for (int i = 0; i < arr.length; i++) {
			int no = arr[i];
			int count = 1;

			for (int j = i + 1; j < arr.length; j++) {
				if (no == arr[j]) {
					count++;
					freq[j] = -1;
				}
			}

			if (freq[i] != -1) {
				freq[i] = count;
			}
		}

		for (int i = 0; i < freq.length; i++) {
			if (freq[i] == 1) {
				System.out.println(arr[i] + " appeared " + freq[i] + " times");
			}
		}

	}

	private void pattern1() {

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a string");
		String no = sc.nextLine();
		for (int i = 0; i < no.length(); i++) {
			for (int j = 0; j < no.length(); j++) {
				if (i == j || i + j == no.length() - 1) {
					System.out.print(no.charAt(j) + " ");

				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}

	}

	private void pattern() {
		String name = "water";
		for (int i = 0; i < name.length(); i++) {
			for (int j = 0; j < name.length(); j++) {
				if (i == j || i + j == name.length() - 1)
					System.out.print(name.charAt(i) + " ");

				else
					System.out.print("  ");
			}
			System.out.println();
		}
		System.out.println(
				"-------------------------------------------------------------------------------------------------");

	}

}