package Learnscanner;
import java.util.Scanner;
public class Lscannerarr {
	
	  public static void main(String[] args) {

	    Scanner scanner = new Scanner(System.in);

	    // Get size of 3D array from user
	    System.out.print("Enter size for X dimension: ");
	    int x = scanner.nextInt();
	    System.out.print("Enter size for Y dimension: ");  
	    int y = scanner.nextInt();
	    System.out.print("Enter size for Z dimension: ");
	    int z = scanner.nextInt();

	    // Create 3D array
	    String[][][] arr = new String[x][y][z];

	    // Take string input from user to populate array
	    for(int i=0; i<x; i++) {
	      for(int j=0; j<y; j++) {
	        for(int k=0; k<z; k++) {
	          System.out.print("Enter string for "+i+","+j+","+k+": ");
	          arr[i][j][k] = scanner.next(); 
	        }  
	      }
	    }

	    // Print array
	    System.out.println("3D array contains:");
	    for(int i=0; i<x; i++) {
	      for(int j=0; j<y; j++) { 
	        for(int k=0; k<z; k++) {
	          System.out.print(arr[i][j][k] + " "); 
	        }
	        System.out.println();  
	      }
	    }

	  }
	}


