package learnFileIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystemNotFoundException;

public class FileDemo {

	public static void main(String[] args) throws IOException {

		FileDemo fd = new FileDemo();
		//fd.sample();
		fd.stream();

	}

	private void stream() throws IOException {
		
		File input=new File("/home/santhosh/Pictures/Screenshot from 2023-08-23 15-05-14.png");
		File output=new File("/home/santhosh/Downloads/wallpaper.png");
		FileInputStream reader = new FileInputStream(input);
		FileOutputStream writer = new FileOutputStream(output);
		
		int i=reader.read();
		while(i!=-1) {
			writer.write(i);
			i=reader.read();
		}
		
	}

	private void sample() {

		// list()

		File note = new File("/home/santhosh/GitFrontend/frontenddev/html");

		String[] file_folder = note.list();
		for (int i = 0; i < file_folder.length; i++) {
			if (file_folder[i].endsWith(".css")) {
				System.out.println(file_folder[i]);
			}
		}
		System.out.println("-----------------------------------------------------------------------------------");
		// list files()
		File[] ff = note.listFiles();
		for (int i = 0; i < ff.length; i++) {
			if (ff[i].isDirectory())
				// if(ff[i].isFile())
				System.out.println(ff[i].getName());
		}
	System.out.println("-------------------------------------------------------------------------------------------");
		
	File note1 = new File("/home/santhosh/GitFrontend/frontenddev/html/About.html");
		
		try (FileReader reader = new FileReader(note1)) {
			BufferedReader br=new BufferedReader(reader);
			String line=br.readLine();
			while(line!=null){       // its return tupe is String so we used null
				System.out.println(line);
				line=br.readLine();
			}
			/*int i=reader.read();
			while(i!=-1)    // its return tupe is integer so we used -1
			{
				System.out.print((char)i);
				i=reader.read();     
				}*/
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FileSystemNotFoundException io) {
			io.printStackTrace();
		}
		
		
		}

	}


