package swithcase;

public class Switchcase {

	public static void main(String[] args) {

		// ternary operator

		int marks = 100;
		String result = marks >= 50 ? "pass" : "fail";
		System.out.println(result);

		int a, b, c, no;
		String operation = "multiplication";
		switch (operation) {
		case "addition":
			a = 10;
			b = 20;
			c = a + b;
			System.out.println(c + " " + "addition");
			break;
		case "subtraction":
			a = 10;
			b = 20;
			c = a - b;
			System.out.println(c + " " + "subtraction");
			break;
		case "multiplication":
			a = 10;
			b = 20;
			c = a * b;
			System.out.println(c + " " + "multiplication");
			break;
		case "division":
			a = 10;
			b = 20;
			c = a / b;
			System.out.println(c + " " + "division");
			break;
		case "oe":
			no = 376;
			if (no % 2 == 0) {
				System.out.println("it was an even value" + " " + no);

			} else
				System.out.println("odd value" + " " + no);
			break;
		default:
			System.out.println("thank you");
			break;

		}

	}
}
