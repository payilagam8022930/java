package learnloop;

public class Learnloop2 {
	public static void main(String[] args)
	{
		Learnloop2 ll=new Learnloop2();
		/*ll.loop1();
		ll.loop2();
		ll.loop3();
		ll.loop4();
		ll.loop5();   //while loop
		ll.loop6();  //for loop */
		//ll.loop7();
		//ll.loop8();
		ll.loop9();
		//ll.loop10();    //continue and break
		//ll.loop11(); //do while
		
	}
	private void loop11() {
		
		int no=1;
		do
		{
			System.out.println(no);
			no++;
		} while(no<=5);
		
	}
	private void loop10() {
		for(int no=1;no<=11;no++)
		{
			if(no==10)
			{
				continue;
			}
			System.out.println(no);
		}
		
	}
	private void loop9() 
	
	{
		for(int row=1;row<=5;row++) {           //rows
			for(int column=1;column<=5;column++) {				//columns
				System.out.print(" "+ row);
			}
			
			System.out.println();
			
		}}
	private void loop8() {
	for(int no=1;no<=5;no++)
	{
		System.out.print(no+" ");
	}
	System.out.println();
	for(int no=1;no<=5;no++)
	{
		System.out.print(no+" ");
	}
	System.out.println();	
	for(int no=1;no<=5;no++)
	{
		System.out.print(no+" ");
	}
	System.out.println();	
	for(int no=1;no<=5;no++)
	{
		System.out.print(no+" ");
	}
	System.out.println();
	for(int no=1;no<=5;no++)
	{
		System.out.print(no+" ");
	}
	}
	
	private void loop7() {
		int num=5;
		System.out.println(++num);
		System.out.println(num);
		
	}
	private void loop6() {
		for(int no=1;no<=5;no++)
			System.out.print(no+" ");
		
	}
	private void loop5() {
		int count=15;
		while (count>=0) {
			System.out.println(count+" ");
			count=count-3;
		}
	}
	private void loop4() {
		int count=5;
		while (count>=-1) {
			System.out.print(count+" ");
			count--;
		}
		
	}
	private void loop3() {
		int count=5;
		while (count>=1) {
			System.out.println(count+" ");
			count--;
		}
	}
	private void loop2() {
		int count=1; 						//start
		while (count<=5)				 //end
		{
			System.out.println("hi"+" ");
			count++;					 //iteration
		}
		
	}
	private void loop1() {
		int count=1; 						//start
		while (count<=5)				 //end
		{
			System.out.print("hi"+" ");
			count++;					 //iteration
		}
		}
}

