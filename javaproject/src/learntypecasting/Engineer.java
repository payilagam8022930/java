package learntypecasting;

public class Engineer extends Student{    //type cast in non-primitive data type

	public static void main(String[] args) {

//-------------Up casting---------------------------------------------------------------		
		
		Engineer child=new Engineer();
		child.doProjects();        
		child.study();   
		String name="abc";
		Student parent=(Student)child; //up casting or widen casting //parent converted to child class
		parent.study();
		
		System.out.println(child.hashCode());
		System.out.println(name.hashCode()); // string is another class so # code was not changed in the console 
	
//-------------Down casting---------------------------------------------------------------
	
		Student ss=new Engineer();
		Engineer ee= (Engineer)ss;  //down casting //child converted to parent class
		ee.doProjects();
		ee.study();
		
	}
	
	public int hashCode(){    //hash code was in string class
		return 123;           //over riding #code() method 123
	}

	private void doProjects() {
	
		System.out.println("do projects");
		
	}
	
	

}
