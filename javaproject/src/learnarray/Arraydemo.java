package learnarray;

public class Arraydemo {

	public static void main(String[] args) {

		Arraydemo ad = new Arraydemo();

		// ad.type1();
		// ad.type2();
		// ad.type3();
		// ad.type4();
		// ad.type7(); //for each loop
		//ad.arrsum();
		// ad.operator();
		 ad.marklist();

	}

	private void marklist() {

		int marks[] = { 56, 6, 88, 29, 31, 78 };
		int total = 0;
		for (int index = 0; index < marks.length; index++)
			total = total + marks[index];
		System.out.println("Total => " +total);
		int avg=total/marks.length;
				System.out.println("Average => " + avg);
	
		if(avg>90 && avg<=100) {
			System.out.println("A+");
		}
		else if(avg>80 && avg<=90) {
			System.out.println("A");
		}
		else if(avg>70 && avg<=80) {
			System.out.println("B+");
		}
		else if(avg>60 && avg<=70) {
			System.out.println("B");
		}
		else if(avg>50 && avg<=60) {
			System.out.println("C+");
		}
		else if(avg>35 && avg<=50) {
			System.out.println("C");
		}
		else {
			System.out.println("below 35 fail");
		}
			
				
	}
	private void operator() {

		int n = 2, n1 = 5, n2 = 7, n3 = 9;
		if (n < n2 && n3 > n1) {
			System.out.println("yes");
		}

	}

	private void arrsum() {

		int marks[] = { 97, 75, 90, 25, 42 };
		int total = 0;
		for (int index = 0; index < marks.length; index++)
			total = total + marks[index];
		System.out.println(total);

	}

	private void type7() {

		int[] marks = new int[5];

		marks[0] = 95;
		marks[1] = 9;
		marks[2] = 6;
		marks[3] = 4;
		marks[4] = 9;

		for (int mrk : marks) {

			// if(mrk>90)
			System.out.println(mrk);

		}
	}

	private void type4() {

		int[] marks = new int[5];

		marks[0] = 00;
		marks[1] = 11;
		marks[2] = 22;
		marks[3] = 33;
		marks[4] = 44;

		for (int index = marks.length - 1; index >= 0; index--) {
			System.out.println(marks[index]);

		}

	}

	private void type3() {

		int[] marks = new int[5];

		marks[0] = 96;
		marks[1] = 9;
		marks[2] = 6;
		marks[3] = 4;
		marks[4] = 9496;

		for (int index = 0; index < marks.length; index++) {
			if (index % 2 == 0)
				System.out.println(marks[index]);

		}

	}

	private void type2() {

		int[] marks = new int[5];

		marks[0] = 96;
		marks[1] = 9;
		marks[2] = 6;
		marks[3] = 4;
		marks[4] = 9496;

		System.out.println("even values");
		for (int i = 0; i < marks.length; i++) {
			if (marks[i] % 2 == 0)
				System.out.println(marks[i]);

		}
		System.out.println("odd values");
		for (int i = 0; i < marks.length; i++) {
			if (marks[i] % 2 != 0)
				System.out.println(marks[i]);
		}

	}

	private void type1() {

		int[] marks = { 97, 75, 90, 25, 43 };

		for (int index = 0; index < marks.length; index++) {
			System.out.println(marks[index]);

		}

	}

}
