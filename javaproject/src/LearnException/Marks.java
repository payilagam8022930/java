package LearnException;

import java.io.IOException;

public class Marks { // Exam.java

	public static void main(String[] args) {

		int[] marks = { 23, 67, 43, 90, 65, 67 };

		try {
			calculate(marks);
		}catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void calculate(int[] marks)
			throws ArrayIndexOutOfBoundsException, NegativeArraySizeException, IOException {

		for (int i = 0; i <= 5; i++) {
			System.out.println(marks[i]);
		}
	}

}
