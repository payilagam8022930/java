package LearnException;

import java.io.IOException;

public class Exam {

	public static void main(String[] args) {
		
		LowAttendanceEx ae = new LowAttendanceEx();
		
		try {
			ae.allow(74);
			System.out.println("hi");
		} catch (LowAttendanceEx ls) {
			System.out.println(ae.getMessage());
		}
		
		
		int[] marks = { 34, 98, 02, 76, 23 };
		try {
			Marks.calculate(marks);
		} catch (ArrayIndexOutOfBoundsException aa) {
			// System.out.println(aa.getMessage());
			// aa.printStackTrace();
			System.out.println("exception out of bound");
		} catch (IOException eq) {

		}

	}

}
