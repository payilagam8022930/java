package LearnException;

import java.util.Scanner;

public class LearnExceptionHand {

	public static void main(String[] args) {

		LearnExceptionHand le = new LearnExceptionHand();

		Scanner sc = new Scanner(System.in);
		System.out.println("enter 2 numbers :");

		int no1 = sc.nextInt();
		int no2 = sc.nextInt();

		le.divide(no1, no2);
		le.add(no1, no2);

	}

	private void add(int no1, int no2) {

		//System.out.println(no1 + no2);

	}

	private void divide(int no1, int no2) {
		try { // error possible area - block
			System.out.println(no1 / no2);
			int ar[] = new int[no1];
			for (int i = 0; i < 10; i++) {
				System.out.println(ar[i]);
			}
		} catch (ArithmeticException ae) { // error handling area - block
			System.out.println("Check no 2");
		} catch (ArrayIndexOutOfBoundsException y) {
			System.out.println("array is out of bound");
		} catch (Exception e) { // should be used in end of the line
			System.out.println("something wrong");
		}finally {
			//code cleaning area - block
			System.out.println("finally");
		}
	}

}
