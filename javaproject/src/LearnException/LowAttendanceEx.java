package LearnException;

public class LowAttendanceEx extends RuntimeException {

	public void allow(int atten) {
		if (atten >= 75) {
			System.out.println("permit");
		} else {
			// LowAttendanceEx ae=new LowAttendanceEx();
			throw new LowAttendanceEx();
		}	}
	
	public String getMessage() {
		return "Low-Attendance";
				
	}
	
}


