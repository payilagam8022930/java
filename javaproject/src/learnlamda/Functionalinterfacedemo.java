package learnlamda;

@FunctionalInterface
public interface Functionalinterfacedemo {
	
	public static void main(String[] args) {
		
		Functionalinterfacedemo.dd();
		
	}
	
	public int add(int no1,int no2);
	
	static void dd()
	{
		System.out.println("static method");
	}
	
	default void san()
	{
		System.out.println("default method");
	}

	//only one abstract method should be allowed
	
}

